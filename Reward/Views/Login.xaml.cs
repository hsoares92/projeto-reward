﻿using Reward.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Reward
{
    public class Usuario
    {
        public string Email { get; set; }
        public string Senha { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Rua { get; set; }

    }



    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class Login : ContentPage
    {

        public Usuario Usuario { get; set; }

        public Login()
        {
            this.Usuario = new Usuario();
            this.BindingContext = this;
            InitializeComponent();
        }

        private void Button_Logar(object sender, EventArgs e)
        {
            Console.WriteLine("========");
            Console.WriteLine(this.Usuario.Email);
            Console.WriteLine(this.Usuario.Senha);
        }

        private void Button_Cadastrar(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Cadastro());
        }
    }
}

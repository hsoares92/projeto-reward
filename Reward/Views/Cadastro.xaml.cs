﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Reward.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Cadastro : ContentPage
    {
        public Usuario Usuario { get; set; }
        public Cadastro()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine(this.Usuario.Nome);
            Console.WriteLine(this.Usuario.Email);
            Console.WriteLine(this.Usuario.Senha);
            Console.WriteLine(this.Usuario.Telefone);
            Console.WriteLine("Cadastro feito com sucesso", "sucesso", "ok");

            Navigation.PushAsync(new Login());
        }
    }
}